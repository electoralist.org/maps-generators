# inspired in https://gist.github.com/frankrowe/6071443
# usage:
# python3 maps-generator/generate_california_maps.py
import shapefile
from json import dumps

# read the shapefile
reader = shapefile.Reader("data/CA/CD_Final 2021-12-20.shp")
fields = reader.fields[1:]
field_names = [field[0] for field in fields]
for sr in reader.shapeRecords():
    buffer = []
    atr = dict(zip(field_names, sr.record))
    geom = sr.shape.__geo_interface__
    buffer.append(dict(type="Feature", geometry=geom, properties=atr)) 
    geojson = open(f"output/CA-{atr['DISTRICT_N']}.json", "w")
    geojson.write(dumps({"type": "FeatureCollection", "features": buffer}) + "\n")
    geojson.close()
